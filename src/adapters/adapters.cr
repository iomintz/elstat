alias AdapterValue = (Bool | Int64 | String?)

class AdapterResult
  property status : Bool
  property latency : Int64
  property error : String?

  def initialize(@status, @latency : Int64, @error = nil)
    if !@error.nil?
      @status = false
      @latency = 0
    end
  end
end

class FullAdapterResult < AdapterResult
  property timestamp : Int64

  def initialize(@timestamp, @status, @latency, @error = nil)
  end

  def initialize(@timestamp : Int64, result : AdapterResult)
    @status = result.status
    @latency = result.latency
    @error = result.error
  end
end

class AdapterError < Exception
end
