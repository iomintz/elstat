require "kemal"
require "openssl"

require "./helpers"

macro do_login
  auth = env.request.headers["Authorization"]?
  if auth.nil?
    halt env, status_code: 401, response: "Unauthorized"
  end

  manager = get_manager(env)
  cfg = manager.@context.cfg
  orig_pwd = cfg["elstat"]["password"]

  hash = OpenSSL::Digest.new("SHA256")
  hash.update(orig_pwd)
  orig_hash = hash.hexdigest

  if auth != orig_hash
    halt env, status_code: 401, response: "Invalid password"
  end
end
