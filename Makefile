all: release

clean:
	rm -f release.tar release.tar.gz

release:
	shards build --progress --release --static
	tar cvf ./release.tar ./bin/elstat ./config.example.ini
	xz ./release.tar
